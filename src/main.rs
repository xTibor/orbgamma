extern crate orbclient;

use orbclient::{Color, Window, Renderer, EventOption};

const WIDTH: usize = 640;
const HEIGHT: usize = 240;

#[inline]
fn linear_to_srgb(channel: f64) -> u8 {
    let result = if channel <= 0.0031308 {
        channel * 12.92
    } else {
        1.055 * channel.powf(1.0 / 2.4) - 0.055
    };
    (result * 255.0) as u8
}

#[inline]
fn srgb_to_linear(channel: u8) -> f64 {
    let channel = channel as f64 / 255.0;
    if channel <= 0.04045 {
        channel / 12.92
    } else {
        ((channel + 0.055) / 1.055).powf(2.4)
    }
}

fn main() {
    let mut window = Window::new(-1, -1, WIDTH as u32, HEIGHT as u32, "Gradient").unwrap();

    let start_color = Color::rgb(0, 255, 0);
    let end_color = Color::rgb(0, 0, 255);

    // sRGB
    window.linear_gradient(0, 0, WIDTH as u32, HEIGHT as u32 / 2, 0, 0,  WIDTH as i32, 0, start_color, end_color);

    // Linear
    for x in 0..WIDTH {
        fn interp(start_color: u8, end_color: u8, scale: f64) -> u8 {
            linear_to_srgb(srgb_to_linear(start_color) * (1.0 - scale) + srgb_to_linear(end_color) * scale)
        }

        let scale = x as f64 / WIDTH as f64;
        let r = interp(start_color.r(), end_color.r(), scale);
        let g = interp(start_color.g(), end_color.g(), scale);
        let b = interp(start_color.b(), end_color.b(), scale);
        let a = interp(start_color.a(), end_color.a(), scale);
        let color = Color::rgba(r, g, b, a);

        window.line(x as i32, HEIGHT as i32 / 2, x as i32, HEIGHT as i32, color);
    }

    window.sync();

    'events: loop {
        for event in window.events() {
            match event.to_option() {
                EventOption::Quit(_quit_event) => break 'events,
                _ => { },
            }
        }
    }
}
